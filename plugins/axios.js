export default function ({ $axios, $auth, redirect, store }) {
    $axios.onRequest((config) => {
        config.headers = {
            "Content-Type": "text/plain"
        /*'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Accept': 'application/json',*/
        //'Access-Control-Allow-Headers': 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin',
        //'Authorization': store.state.auth.tokenlocal, // refers to nuxt.config.js->auth.token
        }
    })
    
        $axios.onError((error) => {
            if (error.response.status === 500) {
                redirect('/error')
            }
        })
    }