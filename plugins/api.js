import Authentification from "~/services/auth";

export default (context, inject) => {
    //context.$axios.setHeader("Content-Type", "text/plain");

    // Initialize API factories
    const factories = {
        auth: Authentification(context.$axios)
      };

    // Inject $api
    inject("api", factories);
};
