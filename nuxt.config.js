export default {
    head: {
        title: "Conso Invest Admin",
        htmlAttrs: {
            lang: "en"
        },
        meta: [
            { charset: "utf-8" },
            {
                name: "viewport",
                content: "width=device-width, initial-scale=1"
            },
            {
                hid: "description",
                name: "description",
                content: "Conso Invest Admin"
            }
        ],
        link: [
            {
                rel: "stylesheet",
                href:
                    "https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700&amp;amp;subset=latin-ext"
            },
            {
                rel: "shortcut icon",
                href: "~/static/favicon.png"
            },
            {
                rel: "apple-touch-icon-precomposed",
                href: "~/static/favicon.png"
            }
        ]
    },

    css: [
        "~/static/fonts/Linearicons/Font/demo-files/demo.css",
        "~/static/fonts/font-awesome/css/font-awesome.min.css",
        "~/static/css/bootstrap.min.css",
        "~/assets/styles/env.scss",
        "~/assets/styles/style.scss"
    ],

    buildModules: ["@nuxtjs/vuetify", "@nuxtjs/style-resources"],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/auth-next'
      ],
      auth: {
        strategies: {
            local: {
              user: {
                property: 'user',
                autoFetch: true
              },
              endpoints: {
                login: { url: 'user.authentification', method: 'post' },
                logout: { url: '/api/auth/logout', method: 'post' },
                user: { url: '/api/auth/user', method: 'get', propertyName: false }
              },
            }
          },
          localStorage: {

            prefix: "auth."
         
          },
      },
      toast: {
        position: 'top-right',
        duration: 2000
      },
    plugins: [
        "~/plugins/popper.js",
        '~/plugins/axios.js',
        '~/api/init.js',
        { src: '~/plugins/vuex-toasted', ssr: false },
        { src: '~/plugins/vuex-persist', ssr: false },
        { src: "~/plugins/jquery.js", ssr: false },
        { src: "~/plugins/bootstrap.js", ssr: false },
        { src: "~/plugins/apexcharts.js", ssr: false }],
    styleResources: {
        scss: "./assets/scss/env.scss"
    },
    router: {
        linkActiveClass: "",
        linkExactActiveClass: "active",
        middleware: ['auth-redirect']
    },
    axios: {
        baseURL: 'https://consoinvest.mboahosting.com/api/'
    },
    server: {
        port: 4003,
        host: "localhost"
    }
};
