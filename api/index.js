import axios from 'axios'
const headers = {
  "Content-Type": "text/plain"
};
export default {
  auth: {
    me: () => axios.get('auth/me'),
    login: (data) => axios.post('user.authentification', data),
    register: (data) => axios.post('user.create', data),
    activate: (data) => axios.post('user.activateaccount?user_id='+data.id, data),
  },
  country: {
    list: () => axios.get('lazyloading.country'),
  },
  bonachat: {
    create: (data) => axios.post('create.bonachat', data, {
      headers: headers
  }),
    list: () => axios.get('lazyloading.bonachat?dfilters=on&next=1&per_page=100'),
    update: (data) => axios.post('update.bonachat?id='+data.bonachat.id, data, {
      headers: headers
  }),
    get: (data) => axios.post('detail.typebonachat?id='+data.id),
    delete: (data) => axios.post('delete.bonachat?id='+data.id),
    upload: (data) => axios.post('uploads.fichier', data)  
  },
  userrequest: {
    create: (data) => axios.post('create.userrequest', data, {
      headers: headers
  }),
    list: () => axios.get('lazyloading.userrequest?dfilters=on&next=1&per_page=100'),
    update: (data) => axios.post('update.userrequest?id='+data.userrequest.id, data, {
      headers: headers
  }),
    get: (data) => axios.post('detail.userrequest?id='+data.id),
    delete: (data) => axios.post('delete.userrequest?id='+data.id),
    upload: (data) => axios.post('uploads.fichier', data)  
  },
  card: {
    create: (data) => axios.post('create.carte', data, {
      headers: headers
  }),
    list: () => axios.get('lazyloading.carte?dfilters=on&next=1&per_page=100'),
    update: (data) => axios.post('update.carte?id='+data.carte.id, data, {
      headers: headers
  }),
    get: (data) => axios.post('detail.carte?id='+data.id),
    delete: (data) => axios.post('delete.carte?id='+data.id),
    upload: (data) => axios.post('uploads.fichier', data)  
  },
  typebon: {
    create: (data) => axios.post('create.typebonachat', datacc),
    list: () => axios.get('lazyloading.typebonachat?dfilters=on&next=1&per_page=100'),
    update: (data) => axios.post('update.typebonachat?id='+data.id, data),
    delete: (data) => axios.post('delete.typebonachat?id='+data.id),
    get: (data) => axios.post('detail.typebonachat?id='+data.id),
  },
  typecard: {
    create: (data) => axios.post('create.typecarte', data, {
      headers: headers
    }),
    list: () => axios.get('lazyloading.typecarte?dfilters=on&next=1&per_page=100'),
    update: (data) => axios.post('update.typecarte?id='+data.id, data),
    delete: (data) => axios.post('delete.typecarte?id='+data.id),
    get: (data) => axios.post('detail.typecarte?id='+data.id),
  },
  categoryservice: {
    create: (data) => axios.post('create.categoryservice', data, {
      headers: headers
    }),
    list: () => axios.get('lazyloading.categoryservice?dfilters=on&next=1&per_page=1000'),
    update: (data) => axios.post('update.categoryservice?id='+data.id, data),
    delete: (data) => axios.post('delete.categoryservice?id='+data.id),
    get: (data) => axios.post('detail.categoryservice?id='+data.id),
  },
  demandeservice: {
    create: (data) => axios.post('create.demandeservice', data, {
      headers: headers
    }),
    list: () => axios.get('lazyloading.demandeservice?dfilters=on&next=1&per_page=1000'),
    update: (data) => axios.post('update.demandeservice?id='+data.demandeservice.id, data, {
      headers: headers
    }),
    delete: (data) => axios.post('delete.demandeservice?id='+data.demandeservice.id),
    get: (data) => axios.post('detail.demandeservice?id='+data.id),
  },
  service: {
    create: (data) => axios.post('create.service', data, {
      headers: headers
    }),
    list: () => axios.get('lazyloading.service?dfilters=on&next=1&per_page=1000'),
    update: (data) => axios.post('update.service?id='+data.id, data),
    delete: (data) => axios.post('delete.service?id='+data.id),
    get: (data) => axios.post('detail.service?id='+data.id),
  },
  order: {
    create: (data) => axios.post('create.paiement', data),
    list: () => axios.get('lazyloading.paiement?dfilters=on&next=1&per_page=all'),
    update: (data) => axios.post('update.paiement?id='+data.id, data),
    delete: (data) => axios.post('delete.paiement?id='+data.id),
    get: (data) => axios.post('detail.paiement?id='+data.id),
    getbyuser: (data) => axios.post('lazyloading.paiement?dfilters=on&user.id:eq='+data),
  },
  user: {
    create: (data) => axios.post('user.create', data, {
      headers: headers
  }),
    histocaisse: (data) => axios.get('lazyloading.paiement?dfilters=on&next=1&per_page=all&caissiere_id:eq='+data),
    paiements: (data) => axios.get('lazyloading.paiement?dfilters=on&next=1&per_page=all&user.id:eq='+data),
    list: () => axios.get('lazyloading.user?dfilters=on&next=1&per_page=all'),
    caisses: () => axios.get('lazyloading.user?dfilters=on&next=1&per_page=all&role:eq=2'),
    revendeurs: () => axios.get('lazyloading.user?dfilters=on&next=1&per_page=all&role:eq=3'),
    update: (data) => axios.post('update.user?id='+data.user.id, data, {
      headers: headers
  }),
    delete: (data) => axios.post('delete.user?id='+data.id),
    get: (data) => axios.post('detail.user?id='+data.id),
    rechargerevendeur: (data) => axios.post('recharge.revendeur', data, {
      headers: headers
  }),
  },
}