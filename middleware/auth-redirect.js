/*export default function ({ store, redirect }) {
    console.log(store.getters['isAuthenticated']);
    if (!store.getters['isAuthenticated']) {
        return redirect('/authenticate/login')
      }
  }*/

  export default function ({ store, redirect, route }) {
    // if the user is not authenticated, and we are not on the login or sign up page then redirect to the login page.
    if (!store.getters['user/isAuthenticated'] && !['/authenticate/login'].includes(route.path)) {
      return redirect('/authenticate/login')
    // if the user is authenticated, and we are not on the home page then redirect to the home page.
    } else if (store.getters['user/isAuthenticated'] && route.path !== '/') {
      return route.path
    }
  }