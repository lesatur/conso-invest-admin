import api from '~/api'
export const state = () => ({
  edit:false,
  demandeservice: null,
  demandeservices: []
})
export const mutations = {
    set_demandeservices (store, data) {
        store.demandeservices = data
    },
  set_demandeservice (store, data) {
    store.demandeservice = data
  },
  new_demandeservice(store, data) {
    store.demandeservices.push(data)
  },
  delete_demandeservice(store, data){
    store.demandeservices.splice(data, 1);
  },
  edit_demandeservice(store, data){
    store.demandeservice = data;
    store.edit = true
  },
  update_demandeservice(store, data){
    store.demandeservice = data;  
    store.edit = false
  },
  reset_demandeservice (store) {
    store.demandeservice = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
      //alert('hllo')
    return api.demandeservice.list()
      .then(response => {
        commit('set_demandeservices', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_demandeservice')
        return error
      })
  },
  create ({commit}, data) {
    data.demandeservice['user.id'] = data.demandeservice.user
    delete data.demandeservice.user

    data.demandeservice['categoryservice.id'] = data.demandeservice.categoryservice
    delete data.demandeservice.categoryservice
    return api.demandeservice.create(data)
      .then(response => {
        console.log(response.data.demandeservice)
        commit('new_demandeservice', response.data.demandeservice)
        return response
      })
  },
  update ({commit}, data) {
    data.demandeservice['user.id'] = data.demandeservice.user
    delete data.demandeservice.user

    data.demandeservice['categoryservice.id'] = data.demandeservice.categoryservice
    delete data.demandeservice.categoryservice
    return api.demandeservice.update(data)
      .then(response => {
        commit('set_demandeservice', response.data.demandeservice)
        commit('reset_demandeservice')
        return response
      })
  },
  delete ({commit}, data) {
    return api.demandeservice.delete(data.id)
      .then(response => {
        commit('delete_demandeservice', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.demandeservice.get(data)
      .then(response => {
        commit('set_demandeservice', response.data.demandeservice)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_demandeservice')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.demandeservices ? state.demandeservices : [];
    },
    edit(state) {
        return state.edit;
    }
};
