import api from '~/api'
export const state = () => ({
  edit:false,
  order: null,
  orders: []
})
export const mutations = {
    set_orders (store, data) {
        store.orders = data
    },
  set_order (store, data) {
    store.order = data
  },
  new_order(store, data) {
    store.orders.push(data)
  },
  delete_order(store, data){
    store.orders.splice(data, 1);
  },
  edit_order(store, data){
    store.order = data;
    store.edit = true
  },
  update_order(store, data){
    store.order = data;  
    store.edit = false
  },
  reset_order (store) {
    store.order = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.order.list()
      .then(response => {
        commit('set_orders', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_order')
        return error
      })
  },
  create ({commit}, data) {
    data.order['typeorder.id'] = data.order.typeorder
    delete data.order.typeorder
    return api.order.create(data)
      .then(response => {
        commit('new_order', response.data.order)
        return response
      })
  },
  update ({commit}, data) {
    data.order['typeorder.id'] = data.order.typeorder
    delete data.order.typeorder
    return api.order.update(data)
      .then(response => {
        commit('set_order', response.data.order)
        commit('reset_order')
        return response
      })
  },
  delete ({commit}, data) {
    return api.order.delete(data.id)
      .then(response => {
        commit('delete_order', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.order.get(data)
      .then(response => {
        commit('set_order', response.data.order)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_order')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.orders ? state.orders : [];
    },
    edit(state) {
        return state.edit;
    },
    bon(state) {
        return state.order? state.order : {};
    },
};
