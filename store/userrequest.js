import api from '~/api'
export const state = () => ({
  userrequest: null,
  image:null,
  userrequests: []
})
export const mutations = {
  set_image(store, data) {
      store.image = data
  },
    set_userrequests (store, data) {
        store.userrequests = data
    },
  set_userrequest (store, data) {
    store.userrequest = data
  },
  new_userrequest(store, data) {
    store.userrequests.push(data)
  },
  delete_userrequest(store, data){
    store.userrequests.splice(data, 1);
  },
  edit_userrequest(store, data){
    store.userrequest = data;
    store.edit = true
  },
  update_userrequest(store, data){
    store.userrequest = data;  
    store.edit = false
  },
  reset_userrequest (store) {
    store.userrequest = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.userrequest.list()
      .then(response => {
        commit('set_userrequests', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_userrequest')
        return error
      })
  },
  create ({commit}, data) {
    data.userrequest['service.id'] = data.userrequest.service
    delete data.userrequest.service

    data.userrequest['user.id'] = data.userrequest.user
    delete data.userrequest.user
    return api.userrequest.create(data)
      .then(response => {
        commit('new_userrequest', response.data.userrequest)
        return response
      })
  },
  update ({commit}, data) {
    data.userrequest['service.id'] = data.userrequest.service
    delete data.userrequest.service

    data.userrequest['user.id'] = data.userrequest.user
    delete data.userrequest.user
    return api.userrequest.update(data)
      .then(response => {
        commit('set_userrequest', response.data.userrequest)
        commit('reset_userrequest')
        return response
      })
  },
  delete ({commit}, data) {
    return api.userrequest.delete(data.id)
      .then(response => {
        commit('delete_userrequest', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.userrequest.get(data)
      .then(response => {
        commit('set_userrequest', response.data.userrequest)
        return response
      })
  },
  uploadimage({commit}, data) {
    return api.userrequest.upload(data)
      .then(response => {
        commit('set_image', response.data.src)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_userrequest')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.userrequests ? state.userrequests : [];
    },
    bon(state) {
        return state.userrequest ? state.userrequest : {};
    },
};
