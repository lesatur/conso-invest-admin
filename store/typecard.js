import api from '~/api'
export const state = () => ({
  typecard: null,
  typecards: []
})
export const mutations = {
    set_typecards (store, data) {
        store.typecards = data
    },
  set_typecard (store, data) {
    store.typecard = data
  },
  new_typecard(store, data) {
    store.typecards.push(data)
  },
  delete_typecard(store, data){
    store.typecards.splice(data, 1);
  },
  edit_typecard(store, data){
    store.typecard = data;
    store.edit = true
  },
  update_typecard(store, data){
    store.typecard = data;  
    store.edit = false
  },
  reset_typecard (store) {
    store.typecard = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.typecard.list()
      .then(response => {
        commit('set_typecards', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_typecard')
        return error
      })
  },
  create ({commit}, data) {
    return api.typecard.create(data)
      .then(response => {
        console.log(response.data.typecarte)
        commit('new_typecard', response.data.typecarte)
        return response
      })
  },
  update ({commit}, data) {
    data.typecard['typetypecard.id'] = data.typecard.typetypecard
    delete data.typecard.typetypecard
    return api.typecard.update(data)
      .then(response => {
        commit('set_typecard', response.data.typecard)
        commit('reset_typecard')
        return response
      })
  },
  delete ({commit}, data) {
    return api.typecard.delete(data.id)
      .then(response => {
        commit('delete_typecard', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.typecard.get(data)
      .then(response => {
        commit('set_typecard', response.data.typecard)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_typecard')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.typecards ? state.typecards : [];
    },
    bon(state) {
        return state.typecard? state.typecard : {};
    },
};
