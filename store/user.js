import api from '~/api'
export const state = () => ({
    user: null,
    users: [],
    caisses:[],
    revendeurs:[]
  })
  export const mutations = {
    set_caisses (store, data) {
      store.caisses = data
    },
    reset_caisses (store) {
      store.caisses = []
    },
    set_revendeurs (store, data) {
      store.revendeurs = data
    },
    reset_revendeurs (store) {
      store.revendeurs = []
    },
    set_user (store, data) {
      store.user = data
    },
    set_users (store, data) {
      store.users = data
    },
    reset_user (store) {
      store.users = []
    }
  }
  export const actions = {
    list ({commit}) {
      return api.user.list()
        .then(response => {
          commit('set_users', response.data.listEntity)
          return response
        })
        .catch(error => {
          commit('reset_users')
          return error
        })
    },
    histocaisses ({commit},data) {
      return api.user.histocaisse(data)
        .then(response => {
          console.log(response)
          return response
        })
        .catch(error => {
          return error
        })
    },
    paiements ({commit},data) {
      return api.user.paiements(data)
        .then(response => {
          console.log(response)
          return response
        })
        .catch(error => {
          return error
        })
    },
    caisses ({commit}) {
      return api.user.caisses()
        .then(response => {
          commit('set_caisses', response.data.listEntity)
          return response
        })
        .catch(error => {
          commit('reset_caisses')
          return error
        })
    },
    revendeurs({commit}) {
      return api.user.revendeurs()
        .then(response => {
          commit('set_revendeurs', response.data.listEntity)
          return response
        })
        .catch(error => {
          commit('reset_revendeurs')
          return error
        })
    },
    rechargerevendeur ({commit}, data) {
      return api.user.rechargerevendeur(data)
        .then(response => {
          //commit('set_user', response.data.user)
          return response
        })
    },
    resetsolde ({commit}, data) {
      return api.user.resetsolde(data)
        .then(response => {
          //commit('set_user', response.data.user)
          return response
        })
    },
    fetch ({commit}) {
      return api.auth.me()
        .then(response => {
          commit('set_user', response.data.result)
          return response
        })
        .catch(error => {
          commit('reset_user')
          return error
        })
    },
    get ({commit}, data) {
      return api.user.get(data)
        .then(response => {
          return response
        })
    },
    create ({commit}, data) {
      //data.user['adminid'] = state.user.id
      data.user['country.id'] = data.user.country
      delete data.user.country
      return api.user.create(data)
        .then(response => {
          //commit('new_user', response.data.user)
          return response
        })
    },
    update ({commit}, data) {
      return api.user.update(data)
        .then(response => {
          return response
        })
    },
    delete ({commit}, data) {
      return api.user.delete(data.id)
        .then(response => {
          return response
        })
    },
    login ({commit}, data) {
        commit('set_user', data);
    },
    register ({commit}, data) {
      data.user['country.id'] = data.user.country
      delete data.user.country
      return api.auth.register(data)
        .then(response => {
          commit('set_user', response.data.user)
          return response
        })
    },
    activate ({commit}, data) {
      return api.auth.activate(data)
        .then(response => {
          commit('set_user', response.data.user)
          return response
        })
    },
    reset ({commit}) {
      commit('reset_user')
      return Promise.resolve()
    }
  }
  
  export const getters = {
    isAuthenticated(state) {
      return state.user !== null;
    },

    role(state) {
      //return state.user.role;
      let role = "";
      switch (state.user.role) {
        case "0":
          role = "Client";
          break;

        case "1":
          role = "Administrateur";
          break;

        case "2":
          role = "Caissiere";
          break;

        case "3":
          role = "Revendeur";
          break;
      }
      return role;
    },
    
    index(state) {
      return state.user ? state.user.firstname  : null;
    },

    caisses(state) {
      return state.caisses ? state.caisses : [];
    },

    revendeurs(state) {
      return state.revendeurs ? state.revendeurs : [];
    },

    table(state) {
      return state.users ? state.users : [];
    }
  };
  