import api from '~/api'
export const state = () => ({
  card: null,
  image:null,
  cards: []
})
export const mutations = {
  set_image(store, data) {
      store.image = data
  },
    set_cards (store, data) {
        store.cards = data
    },
  set_card (store, data) {
    store.card = data
  },
  new_card(store, data) {
    store.cards.push(data)
  },
  delete_card(store, data){
    store.cards.splice(data, 1);
  },
  edit_card(store, data){
    store.card = data;
    store.edit = true
  },
  update_card(store, data){
    store.card = data;  
    store.edit = false
  },
  reset_card (store) {
    store.card = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.card.list()
      .then(response => {
        commit('set_cards', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_card')
        return error
      })
  },
  create ({commit}, data) {
    data.card['typecarte.id'] = data.card.typecard
    delete data.card.typecard

    data.card['user.id'] = data.card.user
    delete data.card.user
    return api.card.create(data)
      .then(response => {
        commit('new_card', response.data.typecarte)
        return response
      })
  },
  update ({commit}, data) {
    data.card['typecarte.id'] = data.card.typecard
    delete data.card.typecard

    data.card['user.id'] = data.card.user
    delete data.card.user
    return api.card.update(data)
      .then(response => {
        commit('set_card', response.data.typecarte)
        commit('reset_card')
        return response
      })
  },
  delete ({commit}, data) {
    return api.card.delete(data.id)
      .then(response => {
        commit('delete_card', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.card.get(data)
      .then(response => {
        commit('set_card', response.data.typecarte)
        return response
      })
  },
  uploadimage({commit}, data) {
    return api.card.upload(data)
      .then(response => {
        commit('set_image', response.data.src)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_card')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.cards ? state.cards : [];
    },
    bon(state) {
        return state.card ? state.card : {};
    },
};
