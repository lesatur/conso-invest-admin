import api from '~/api'
export const state = () => ({
  edit:false,
  typebon: null,
  typebons: []
})
export const mutations = {
    set_typebons (store, data) {
        store.typebons = data
    },
  set_typebon (store, data) {
    store.typebon = data
  },
  new_typebon(store, data) {
    store.typebons.push(data)
  },
  delete_typebon(store, data){
    store.typebons.splice(data, 1);
  },
  edit_typebon(store, data){
    store.typebon = data;
    store.edit = true
  },
  update_typebon(store, data){
    store.typebon = data;  
    store.edit = false
  },
  reset_typebon (store) {
    store.typebon = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.typebon.list()
      .then(response => {
        commit('set_typebons', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_typebon')
        return error
      })
  },
  create ({commit}, data) {
    return api.typebon.create(data)
      .then(response => {
        console.log(response.data.typebonachat)
        commit('new_typebon', response.data.typebonachat)
        return response
      })
  },
  update ({commit}, data) {
    data.typebon['typetypebon.id'] = data.typebon.typetypebon
    delete data.typebon.typetypebon
    return api.typebon.update(data)
      .then(response => {
        commit('set_typebon', response.data.typebon)
        commit('reset_typebon')
        return response
      })
  },
  delete ({commit}, data) {
    return api.typebon.delete(data.id)
      .then(response => {
        commit('delete_typebon', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.typebon.get(data)
      .then(response => {
        commit('set_typebon', response.data.typebon)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_typebon')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.typebons ? state.typebons : [];
    },
    edit(state) {
        return state.edit;
    },
    bon(state) {
        return state.typebon? state.typebon : {};
    },
};
