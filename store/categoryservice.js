import api from '~/api'
export const state = () => ({
  edit:false,
  categoryservice: null,
  categoryservices: []
})
export const mutations = {
    set_categoryservices (store, data) {
        store.categoryservices = data
    },
  set_categoryservice (store, data) {
    store.categoryservice = data
  },
  new_categoryservice(store, data) {
    store.categoryservices.push(data)
  },
  delete_categoryservice(store, data){
    store.categoryservices.splice(data, 1);
  },
  edit_categoryservice(store, data){
    store.categoryservice = data;
    store.edit = true
  },
  update_categoryservice(store, data){
    store.categoryservice = data;  
    store.edit = false
  },
  reset_categoryservice (store) {
    store.categoryservice = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.categoryservice.list()
      .then(response => {
        commit('set_categoryservices', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_categoryservice')
        return error
      })
  },
  create ({commit}, data) {
    return api.categoryservice.create(data)
      .then(response => {
        console.log(response.data.categoryservice)
        commit('new_categoryservice', response.data.categoryservice)
        return response
      })
  },
  update ({commit}, data) {
    return api.categoryservice.update(data)
      .then(response => {
        commit('set_categoryservice', response.data.categoryservice)
        commit('reset_categoryservice')
        return response
      })
  },
  delete ({commit}, data) {
    return api.categoryservice.delete(data.id)
      .then(response => {
        commit('delete_categoryservice', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.categoryservice.get(data)
      .then(response => {
        commit('set_categoryservice', response.data.categoryservice)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_categoryservice')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.categoryservices ? state.categoryservices : [];
    },
    edit(state) {
        return state.edit;
    },
};
