/*export const state = () => ({
    isLoggedIn: false
});

export const mutations = {
    setIsLoggedIn(state, payload) {
        state.isLoggedIn = payload;
    }
};

export const actions = {
    setAuthStatus({ commit, state }, payload) {
        commit('setIsLoggedIn', payload);
        const cookieParams = {
            isLoggedIn: state.isLoggedIn
        };

        this.$cookies.set('auth', cookieParams, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
        });
    }
};*/

import api from '~/api'
export const state = () => ({
  user: null
})
export const mutations = {
  set_user (store, data) {
    store.user = data
  },
  reset_user (store) {
    store.user = null
  }
}
export const actions = {
  fetch ({commit}) {
    return api.auth.me()
      .then(response => {
        commit('set_user', response.data.result)
        return response
      })
      .catch(error => {
        commit('reset_user')
        return error
      })
  },
  login ({commit}, data) {
    let fd = new FormData()
    fd.append("login", data.login);
    fd.append("password", data.password);
    return api.auth.login(fd)
      .then(response => {
        console.log(response.data.user);
        commit('set_user', response.data.user)
        return response
      })
  },
  register ({commit}, data) {
    data.user['country.id'] = data.user.country
    delete data.user.country
    return api.auth.register(data)
      .then(response => {
        commit('set_user', response.data.user)
        return response
      })
  },
  activate ({commit}, data) {
    return api.auth.activate(data)
      .then(response => {
        commit('set_user', response.data.user)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_user')
    return Promise.resolve()
  }
}

export const getters = {
  authenticated(state) {
    return state.user !== null;
  },

  user(state) {
    return state.user ? state.user : null;
  }
};
