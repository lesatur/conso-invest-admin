import api from '~/api'
export const state = () => ({
  edit:false,
  bonachat: null,
  image:null,
  bonachats: []
})
export const mutations = {
  set_image(store, data) {
      store.image = data
  },
    set_bonachats (store, data) {
        store.bonachats = data
    },
  set_bonachat (store, data) {
    store.bonachat = data
  },
  new_bonachat(store, data) {
    store.bonachats.push(data)
  },
  delete_bonachat(store, data){
    store.bonachats.splice(data, 1);
  },
  edit_bonachat(store, data){
    store.bonachat = data;
    store.edit = true
  },
  update_bonachat(store, data){
    store.bonachat = data;  
    store.edit = false
  },
  reset_bonachat (store) {
    store.bonachat = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.bonachat.list()
      .then(response => {
        commit('set_bonachats', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_bonachat')
        return error
      })
  },
  create ({commit}, data) {
    data.bonachat['typebonachat.id'] = data.bonachat.typebonachat
    delete data.bonachat.typebonachat
    return api.bonachat.create(data)
      .then(response => {
        commit('new_bonachat', response.data.bonachat)
        return response
      })
  },
  update ({commit}, data) {
    data.bonachat['typebonachat.id'] = data.bonachat.typebonachat
    delete data.bonachat.typebonachat
    return api.bonachat.update(data)
      .then(response => {
        commit('set_bonachat', response.data.bonachat)
        commit('reset_bonachat')
        return response
      })
  },
  delete ({commit}, data) {
    return api.bonachat.delete(data.id)
      .then(response => {
        commit('delete_bonachat', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.bonachat.get(data)
      .then(response => {
        commit('set_bonachat', response.data.bonachat)
        return response
      })
  },
  uploadimage({commit}, data) {
    return api.bonachat.upload(data)
      .then(response => {
        commit('set_image', response.data.src)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_bonachat')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.bonachats ? state.bonachats : [];
    },
    edit(state) {
        return state.edit;
    },
    bon(state) {
        return state.bonachat? state.bonachat : {};
    },
};
