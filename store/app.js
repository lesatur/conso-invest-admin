import api from '~/api'
export const state = () => ({
    isDrawer: false,
    isSearch: false
});

export const mutations = {
    toggleDrawer(state, payload) {
        state.isDrawer = payload;
    },

    toggleSearch(state, payload) {
        state.isSearch = payload;
    }
};

export const actions = {
    countries ({commit}) {
    return api.country.list()
      .then(response => {
        return response
      })
      .catch(error => {
        return error
      })
  }
}
