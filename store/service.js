import api from '~/api'
export const state = () => ({
  edit:false,
  service: null,
  services: []
})
export const mutations = {
    set_services (store, data) {
        store.services = data
    },
  set_service (store, data) {
    store.service = data
  },
  new_service(store, data) {
    store.services.push(data)
  },
  delete_service(store, data){
    store.services.splice(data, 1);
  },
  edit_service(store, data){
    store.service = data;
    store.edit = true
  },
  update_service(store, data){
    store.service = data;  
    store.edit = false
  },
  reset_service (store) {
    store.service = null
    store.edit = false
  }
}
export const actions = {
    list ({commit}) {
    return api.service.list()
      .then(response => {
        commit('set_services', response.data.listEntity)
        return response
      })
      .catch(error => {
        commit('reset_service')
        return error
      })
  },
  create ({commit}, data) {
    return api.service.create(data)
      .then(response => {
        console.log(response.data.service)
        commit('new_service', response.data.service)
        return response
      })
  },
  update ({commit}, data) {
    return api.service.update(data)
      .then(response => {
        commit('set_service', response.data.service)
        commit('reset_service')
        return response
      })
  },
  delete ({commit}, data) {
    return api.service.delete(data.id)
      .then(response => {
        commit('delete_service', data)
        return response
      })
  },
  get ({commit}, data) {
    return api.service.get(data)
      .then(response => {
        commit('set_service', response.data.service)
        return response
      })
  },
  reset ({commit}) {
    commit('reset_service')
    return Promise.resolve()
  }
}

export const getters = {
    table(state) {
        return state.services ? state.services : [];
    },
    edit(state) {
        return state.edit;
    },
    bon(state) {
        return state.service? state.service : {};
    },
};
