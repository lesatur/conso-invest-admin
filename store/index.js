export const getters = {
    isAuthenticated(state) {
      return state.auth.user !== null;
    },

    role(state) {
      let role = "";
      switch (state.user.role) {
        case 0:
          role = "Client";
          break;

        case 1:
          role = "Administrateur";
          break;

        case 2:
          role = "Caissiere";
          break;

        case 3:
          role = "Revendeur";
          break;
      }
      return role;
    },
  
    loggedInUser(state) {
      return state.auth.user ? state.auth.user.firstname : null;
    }
  }